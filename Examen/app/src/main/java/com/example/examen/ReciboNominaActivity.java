package com.example.examen;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;

public class ReciboNominaActivity extends AppCompatActivity {
   private TextView lblNumRecibo;
   private TextView lblNombre;
   private EditText txtHorasNormal;
   private EditText txtHorasExtras;
   private RadioButton rdbAuxiliar;
   private RadioButton rdbAlbañil;
   private RadioButton rdbObra;
   private TextView lblimpuestoPor;
   private TextView lblSubtotal;
   private TextView lblimpuesto;
   private TextView lblTotal;
   private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;


   ReciboNomina recibo = new ReciboNomina();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);
        lblNumRecibo = (TextView) findViewById(R.id.lblNumRecibo);
        lblNombre = (TextView) findViewById(R.id.lblNombre);

        lblNombre.setText("Nombre: " + getIntent().getStringExtra("nombre"));
        lblNumRecibo.setText("Num. Recibo: " + String.valueOf(recibo.getNumRecibo()));
//        recibo = lblNumRecibo.setText();

    }
}
