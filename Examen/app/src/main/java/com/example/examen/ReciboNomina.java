package com.example.examen;

import java.io.Serializable;
import java.util.Random;

public class ReciboNomina implements Serializable {
    private int numRecibo;
    private String Nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float ImpuestoPorc;

    public void ReciboNomina(int numRecibo, String Nombre, float horasTrabNormal, float horasTrabExtras, int puesto, float impuestoPorc){
        this.numRecibo = numRecibo;
        this.Nombre = Nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.ImpuestoPorc = impuestoPorc;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        ImpuestoPorc = impuestoPorc;
    }

    public float getImpuestoPorc() {
        return ImpuestoPorc;
    }
    public float calcularSubtotal(){
//        this.horasTrabNormal
        return 0;
    }
    public float calcularImpuesto(){
        return 0;
    }
    public float calcularTotal(){
       float Total = calcularTotal()-calcularImpuesto();
        return Total;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;

    }

    public int getNumRecibo() {
        return new Random().nextInt(500)%1000;
    }
}
